import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-hijo',
  templateUrl: './hijo.component.html',
  styles: []
})
export class HijoComponent implements OnInit {
@Input() contador;
@Output() cambioContador = new EventEmitter<number>();

  constructor() { }

  ngOnInit() {
  }

  multiplicar() {
    this.contador *= 2;
    this.cambioContador.emit(this.contador);
  }

  dividir() {
    this.contador /= 2;
    this.cambioContador.emit(this.contador);
  }

  reseteaContador(evt: any) {
    this.contador = evt;
    this.cambioContador.emit(this.contador);

  }

}
